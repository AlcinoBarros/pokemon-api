/*
    Lista apenas os pokemons com mais de 140 pontos de velocidade (speed)
    A lista deve estar ordenada do mais rápido para o mais lento
*/

var data = require('../data');

module.exports = function (req, res) {
  var result = 'pokemon list';

  //Implementação
  data = data.filter(function (item) {
    return item.speed > 140;
  });
  // :TODO

  //Retorno
  res.json(data);
};
